//
//  OSAnswerModel.m
//  OSChina
//
//  Created by 林涛 on 14/12/27.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSPostsModel.h"
@interface OSPostsModel()
@property (strong,nonatomic) NSDictionary *answer;
@property (strong,nonatomic) NSString *authorStr;
@end

@implementation OSPostsModel

- (id)mapAttributes
{
    NSArray *map = @[@{@"answer":@"answer"},
                     @{@"authorStr":@"author"},
                     @{@"answerCount":@"answerCount"},
                     @{@"identifier":@"id"},
                     @{@"authorid":@"authorid"},
                     @{@"portrait":@"portrait"},
                     @{@"pubDate":@"id"},
                     @{@"pubDate":@"pubDate"},
                     @{@"title":@"title"},
                     @{@"viewCount":@"viewCount"}
                     ];
    return map;
}

- (NSString *)author
{
    if ([self.authorStr length]) {
        self.answerType = @"回帖";
        return self.authorStr;
        
    }else{
        self.answerType = @"回答";
        return self.answer[@"name"];
    }
}

@end
