//
//  OSAnswerModel.h
//  OSChina
//
//  Created by 林涛 on 14/12/27.
//  Copyright (c) 2014年 林涛. All rights reserved.
//


#import "OSBaseModel.h"
@interface OSPostsModel : OSBaseModel

@property (strong, nonatomic ,readonly) NSString *author;
@property (strong, nonatomic) NSString  *answerCount;
@property (strong, nonatomic) NSString  *authorid;
@property (strong, nonatomic) NSString  *identifier;
@property (strong, nonatomic) NSString  *portrait;
@property (strong, nonatomic) NSString  *pubDate;
@property (strong, nonatomic) NSString  *title;
@property (strong, nonatomic) NSString  *viewCount;
@property (strong, nonatomic) NSString  *answerType;
@end
