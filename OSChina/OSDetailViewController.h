//
//  OSInfoDetailViewController.h
//  OSChina
//
//  Created by 林涛 on 14/12/26.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OSNewsModel.h"
@interface OSDetailViewController : UIViewController
@property (strong, nonatomic) OSNewsModel *infoModel;
@property (strong, nonatomic) NSString *urlPath;
@property (strong, nonatomic) NSString *dataType;
@end
