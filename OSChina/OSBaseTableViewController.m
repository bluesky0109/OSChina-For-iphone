//
//  OSBaseTableViewController.m
//  OSChina
//
//  Created by 林涛 on 14/12/26.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSBaseTableViewController.h"
#import "OSNewsViewController.h"
#import "MJRefresh.h"
@interface OSBaseTableViewController ()

@end

@implementation OSBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup
{
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    [self.tableView headerBeginRefreshing];
}

- (void)footerRereshing
{
    
}

- (void)refreshTableView
{
    [self.tableView reloadData];
    [self.tableView headerEndRefreshing];
    [self.tableView footerEndRefreshing];
}

- (void)headerRereshing
{
   
}

@end
