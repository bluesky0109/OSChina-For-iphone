//
//  OSAnswerTableViewCell.m
//  OSChina
//
//  Created by 林涛 on 14/12/27.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSPostsTableViewCell.h"
#import "OSPostsModel.h"
#import <UIImageView+AFNetworking.h>
#define IS_IPHONE UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone

@interface OSPostsTableViewCell()
@property (strong, nonatomic) IBOutlet UIImageView  *portrait;
@property (strong, nonatomic) IBOutlet UILabel      *title;
@property (strong, nonatomic) IBOutlet UILabel      *detialTitlte;
@property (strong, nonatomic) IBOutlet UILabel      *viewCount;
@property (strong, nonatomic) IBOutlet UILabel      *viewType;

@end
@implementation OSPostsTableViewCell

- (void)awakeFromNib {
}

- (void)configWith:(id)item
{
    UIFont *font           = [UIFont systemFontOfSize:IS_IPHONE ?12:21];
    self.model             = [[OSPostsModel alloc]initWithContent:item];
    self.title.text        = self.model.title;
    [self.portrait setImageWithURL:[NSURL URLWithString:self.model.portrait]];
    self.viewCount.text    = self.model.answerCount;
    self.detialTitlte.text = [NSString stringWithFormat:@"%@ 发布于%@", self.model.author, self.model.pubDate];
    self.viewType.text     = self.model.answerType;
    
    self.title.font        = [UIFont systemFontOfSize:IS_IPHONE ?14:21];;
    self.viewCount.font    = font;
    self.detialTitlte.font = [UIFont systemFontOfSize:IS_IPHONE ?10:21];;
    self.viewType.font     = font;
}

+ (NSString *)identifier {
    return @"PostsCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
