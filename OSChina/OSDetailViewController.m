//
//  OSInfoDetailViewController.m
//  OSChina
//
//  Created by 林涛 on 14/12/26.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSDetailViewController.h"
#import "OSNetworkRequest.h"
#import "OSNewsModel.h"

@interface OSDetailViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation OSDetailViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [OSNetworkRequest detailRequestWithURL:self.urlPath detailID:self.infoModel.identifier success:^(id result) {
        OSNewsModel *model = [[OSNewsModel alloc]initWithContent:result[self.dataType]];
        NSString *author_str = [NSString stringWithFormat:@"<a href='http://my.oschina.net/u/%@'>%@</a>&nbsp;发表于&nbsp;%@",model.authoruid, model.authorname,model.pubDate];
        NSString *html = [NSString stringWithFormat:@"<body style='background-color:#EBEBF3'><div id='oschina_title'>%@</div><div id='oschina_outline'>%@</div><hr/><div id='oschina_body'>%@</div>%@</body>", model.title,author_str,model.body,HTML_Bottom];
        [self.webView loadHTMLString:html baseURL:nil];
    } failure:^(NSError *error) {
        
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
