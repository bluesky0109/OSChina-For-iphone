//
//  OSBaseModel.m
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSBaseModel.h"

@implementation OSBaseModel
- (id)initWithContent:(NSDictionary *)json
{
    self = [super init];
    
    if (self) {
        
        [self setModelData:json];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)mapAttributes
{
    return nil;
}

- (SEL)setterMethod:(NSString *)key
{
    NSString *first = [[key substringToIndex:1] capitalizedString];
    NSString *end = [key substringFromIndex:1];
    NSString *setterName = [NSString stringWithFormat:@"set%@%@:", first, end];
    return NSSelectorFromString(setterName);
}

- (void)setModelData:(NSDictionary *)json
{
    NSArray *mapArray = [self mapAttributes];
    for (NSDictionary *map in mapArray) {
        for (id key in map) {
    
            // setter 方法
            SEL sel = [self setterMethod:key];
            // SEL sce = @selector(sel);
            if ([self respondsToSelector:sel]) {
    
                // 得到JSON key
                id jsonKey = [map objectForKey:key];
    
                // 得到JSON value
                id jsonValue = [json objectForKey:jsonKey];

                if(jsonValue){
                    [self performSelector:sel withObject:jsonValue];
                }
            }
        }
    }
}

//- (void)setModelData:(NSDictionary *)json
//{
//    // 建立映射关系
//    NSDictionary *mapDic = [self mapAttributes];
//    
//    for (id key in mapDic) {
//        
//        // setter 方法
//        SEL sel = [self setterMethod:key];
//        // SEL sce = @selector(sel);
//        if ([self respondsToSelector:sel]) {
//            
//            // 得到JSON key
//            id jsonKey = [mapDic objectForKey:key];
//            
//            // 得到JSON value
//            id jsonValue = [json objectForKey:jsonKey];
//            if ([jsonValue isKindOfClass:[NSString class]]) {
//                if ([jsonValue length]) {
//                  [self performSelector:sel withObject:jsonValue];
//                }
//            }else{
//                [self performSelector:sel withObject:jsonValue];
//            }
//        }
//    }
//} // 属性赋值


@end
