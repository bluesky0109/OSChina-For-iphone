//
//  FirstViewController.m
//  OSChina
//
//  Created by 林涛 on 14/12/23.
//  Copyright (c) 2014年 林涛. All rights reserved.
//


#import "MJRefresh.h"
#import "OSNetworkRequest.h"
#import "OSNewsTableViewCell.h"
#import "OSNewsViewController.h"
#import "OSTableViewDataSourceObject.h"
@interface OSNewsViewController ()<NSXMLParserDelegate,UITabBarControllerDelegate>
@property (assign, nonatomic) BOOL reloading;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedController;
@property (strong, nonatomic) OSTableViewDataSourceObject *tableViewDataSourceObject;
@property (assign, nonatomic) NSInteger pageCount;
@end

@implementation OSNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageCount = 20;
    [self setup];
}

- (void)setup
{
    [super setup];
    self.title = @"综合";
    TableViewCellConfigureBlock block = ^(OSNewsTableViewCell *cell, id item,NSIndexPath *indexPath){
        [cell configWith:item];
    };
    self.tableViewDataSourceObject = [[OSTableViewDataSourceObject alloc]initWithItems:[NSMutableArray array] cellIdentifier:[OSNewsTableViewCell identifier] configureCellBlock:block];
    self.tableView.dataSource = self.tableViewDataSourceObject;
}

- (NSString *)segmentTitle
{
    return [self.segmentedController titleForSegmentAtIndex:self.segmentedController.selectedSegmentIndex];
}

#pragma mark 开始进入刷新状态
- (void)headerRereshing
{
    [self requestNews];
}

- (void)footerRereshing
{
    [self requestNews];
}

- (IBAction)search:(id)sender {
//    UISearchController *searchController = [[UISearchController alloc]initWithSearchResultsController:self];
   // searchController.active = YES;
}

- (void)requestNews
{
    NSString *catalog = [NSString stringWithFormat:@"%ld",self.segmentedController.selectedSegmentIndex + 1];
    NSString *pageIndex = [NSString stringWithFormat:@"%ld",self.pageCount / 20];
    [OSNetworkRequest newListRequestWithCount:@"20" catalog:catalog pageIndex:pageIndex success:^(id result) {
        self.pageCount += 20;
        NSDictionary *dic = result[@"newslist"];
        [self.tableViewDataSourceObject.items addObjectsFromArray:dic[@"news"]];
        [self refreshTableView];
    } failure:^(NSError *error) {
        NSLog(@"%@",error.localizedDescription);
    }];
}

- (IBAction)segmentAction:(UISegmentedControl *)sender {
    self.pageCount = 20;
    [self.tableViewDataSourceObject.items removeAllObjects];
    [self.tableView reloadData];
    [self.tableView headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isKindOfClass:[OSNewsTableViewCell class]]) {
        OSNewsTableViewCell *cell = sender;
        UIViewController *vc = segue.destinationViewController;
        vc.title = [NSString stringWithFormat:@"%@详情",[self segmentTitle]];
        [vc setValue:cell.model forKey:@"infoModel"];
        [vc setValue:api_news_detail forKey:@"urlPath"];
        [vc setValue:@"news" forKey:@"dataType"];
    }
}
@end
