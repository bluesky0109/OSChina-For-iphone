//
//  OSInfoTableViewCell.h
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import "OSBaseTableViewCell.h"
@class OSNewsModel;
@interface OSNewsTableViewCell : OSBaseTableViewCell
@property (nonatomic, strong) OSNewsModel *model;
@end
