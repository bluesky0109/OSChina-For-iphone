//
//  main.m
//  OSChina
//
//  Created by 林涛 on 14/12/23.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
