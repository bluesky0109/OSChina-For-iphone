//
//  OSAnswerTableViewCell.h
//  OSChina
//
//  Created by 林涛 on 14/12/27.
//  Copyright (c) 2014年 林涛. All rights reserved.
//


#import "OSBaseTableViewCell.h"
@class OSPostsModel;
@interface OSPostsTableViewCell : OSBaseTableViewCell
@property (nonatomic, strong) OSPostsModel  *model;
@end
