//
//  OSAppConfig.h
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OSAppConfig : NSObject
+ (OSAppConfig *)shareConfig;
@end
