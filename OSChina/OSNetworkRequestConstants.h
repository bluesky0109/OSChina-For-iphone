//
//  OSNetworkRequestConstants.h
//  OSChina
//
//  Created by 林涛 on 14/12/25.
//  Copyright (c) 2014年 林涛. All rights reserved.
//

#ifndef OSChina_OSNetworkRequestConstants_h
#define OSChina_OSNetworkRequestConstants_h
#define HTML_Style @"<style>#oschina_title {color: #000000; margin-bottom: 6px; font-weight:bold;}#oschina_title img{vertical-align:middle;margin-right:6px;}#oschina_title a{color:#0D6DA8;}#oschina_outline {color: #707070; font-size: 12px;}#oschina_outline a{color:#0D6DA8;}#oschina_software{color:#808080;font-size:12px}#oschina_body img {max-width: 300px;}#oschina_body {font-size:16px;max-width:300px;line-height:24px;} #oschina_body table{max-width:300px;}#oschina_body pre { font-size:9pt;font-family:Courier New,Arial;border:1px solid #ddd;border-left:5px solid #6CE26C;background:#f6f6f6;padding:5px;}</style>"
#define HTML_Bottom @"<div style='margin-bottom:60px'/>"
typedef void(^SucceedBlock)(id result);

#define BASE_URL @"http://www.oschina.net/"
#define api_news_list @"action/api/news_list"
#define api_news_detail @"action/api/news_detail"
#define api_post_list @"action/api/post_list"
#define api_post_detail @"action/api/post_detail"
#define api_blog_list @"action/api/blog_list"
#endif
